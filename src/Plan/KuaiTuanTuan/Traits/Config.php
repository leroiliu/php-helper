<?php

namespace MicroCyanHelper\Plan\KuaiTuanTuan\Traits;

trait Config
{

    /**
     * @param $path
     * @param $options
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function http_post($path,$options){
        try {
            $xcx_version = $this->config['http']['xcx_version']??'3.24.388';
            $options['query'] = ( !empty($options['query'])&&is_array($options['query']) ) ? $options['query'] : [];
            $options['query']['xcx_version'] = $options['query']['xcx_version']??$xcx_version;
            if (isset($options['body'])&&is_array($options['body'])){
                $options['body'] = json_encode($options['body'],JSON_UNESCAPED_SLASHES);
            }
            $o = $this->http->request('POST',$path,$options)->getBody()->getContents();
            return json_decode($o,true)??[];
        }catch (\Exception $exception){
            throw $exception;
        }
    }

}