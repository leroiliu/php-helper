<?php

namespace MicroCyanHelper\Plan\KuaiTuanTuan;

use GuzzleHttp\Exception\GuzzleException;
use MicroCyanHelper\Core\Kernel\BaseCore;
use MicroCyanHelper\Plan\KuaiTuanTuan\Helper\Filter;
use Psr\SimpleCache\InvalidArgumentException;


class App extends BaseCore
{
    use \MicroCyanHelper\Plan\KuaiTuanTuan\Traits\Config;

    /**
     * @var Filter
     */
    public $filter = null;
    /**
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $config=[])
    {
        parent::__construct($config);
        $this->filter = new Filter();
    }

    /**
     * @param array $config
     * @return array|mixed
     * @throws GuzzleException
     */
    public function getOrderList(array $config){
        $options = [];
        $options['body'] = [];
        $options['body']['activity_no'] = $config['activity_no']??'';
        $options['body']['page_size'] = $config['per_page']??5;
        $options['body']['page_number'] = $config['current_page']??1;
        return $this->http_post('/api/ktt_order/customer/query/group_order_list',$options);
    }






}