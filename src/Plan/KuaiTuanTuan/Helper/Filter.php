<?php

namespace MicroCyanHelper\Plan\KuaiTuanTuan\Helper;

class Filter
{

    /**
     * @param $data
     * @return array
     */
    public function OrderList(array $data,array $options=[]): array
    {
        if (!isset($data['result']['list'][0]['sub_order_list'])){
            return [
                'total'=>0,
                'list'=>[]
            ];
        }
        $options['with_avatar_size'] = (!empty($options['with_avatar_size'])&&is_numeric($options['with_avatar_size'])&&$options['with_avatar_size']>0)?$options['with_avatar_size']:0;
        $options['create_time'] = (!empty($options['create_time'])&&is_numeric($options['create_time'])&&$options['create_time']>0);
        $data['result']['list'] = array_map(function ($l)use($options){
            if ($options['with_avatar_size']>0){
                $l['avatar_size'] = 0;
                if (is_string($l['avatar'])&&preg_match("/^http/",$l['avatar'])){
                    $l['avatar'] = preg_replace("/\/thumbnail\/\d+x/",'/thumbnail/'.$options['with_avatar_size'].'x',$l['avatar']);
                    $l['avatar_size'] = get_headers($l['avatar'],true)['Content-Length']??0;
                }
            }
            if ($options['create_time']){
                $l['create_time'] = substr($l['created_at'],0,10);
            }
            return $l;
        },$data['result']['list']);
        return $data['result'];
    }


}