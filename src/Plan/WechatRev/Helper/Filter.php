<?php

namespace MicroCyanHelper\Plan\WechatRev\Helper;

class Filter
{

    /**
     * @param $data
     * @return array
     */
    public function WxProfile($data): array
    {
        if (empty($data['obj']['userInfo']['userName']['string'])){
            return [];
        }
        $profile = [];
        $profile['userName'] = $data['obj']['userInfo']['userName']['string'];
        $profile['nickName'] = $data['obj']['userInfo']['nickName']['string']??'';
        $profile['mobile'] = $data['obj']['userInfo']['bindMobile']['string']??'';
        $profile['sex'] = $data['obj']['userInfo']['sex']??0;
        $profile['country'] = $data['obj']['userInfo']['country']??'';
        $profile['province'] = $data['obj']['userInfo']['province']??'';
        $profile['city'] = $data['obj']['userInfo']['city']??'';
        $profile['signature'] = $data['obj']['userInfo']['signature']??'';
        $profile['alias'] = $data['obj']['userInfo']['alias']??'';
        $profile['avatar'] = $data['obj']['userInfoExt']['smallHeadImgUrl']??'';
        $profile['avatar_size'] = 0;
        if (is_string($profile['avatar'])&&preg_match('/^http/',$profile['avatar'])){
            $profile['avatar_size'] = get_headers($profile['avatar'],true)['Content-Length']??0;
        }
        $profile['qrcode'] = $data['qrcode']??'';
        return $profile;
    }

    /**
     * @param array $data
     * @param array $options
     * @return array
     */
    public function ChatRoomMemberDetail(array $data,array $options=[]): array
    {
        if (empty($data['obj']['newChatroomData']['chatRoomMember'])){
            return [];
        }
        $with_avatar_size = false;
        if (!empty($options['with_avatar_size'])&&is_numeric($options['with_avatar_size'])&&$options['with_avatar_size']>0){
            $with_avatar_size = true;
        }
        $detail = [];
        $detail['count'] = $data['obj']['newChatroomData']['memberCount'];
        $detail['chatroom_id'] = $data['obj']['chatroomUserName']??'';
        $detail['members'] = array_map(function ($member)use($with_avatar_size){
            $data = [];
            $data['userName'] = $member['userName']??'';
            $data['nickName'] = $member['nickName']??'';
            $data['displayName'] = $member['displayName']??'';
            $data['avatar'] = $member['smallHeadImgUrl']??'';
            if ($with_avatar_size){
                $data['avatar_size'] = 0;
                if (is_string($data['avatar'])&&preg_match("/^http/",$data['avatar'])){
                    $data['avatar_size'] = get_headers($data['avatar'],true)['Content-Length']??0;
                }
            }
            $data['inviterUserName'] = $member['inviterUserName']??'';
            return $data;
        },$data['obj']['newChatroomData']['chatRoomMember']??[]);
        return $detail;
    }

    /**
     * @param $data
     * @return array
     */
    public function SnsPage($data): array
    {
        if (empty($data['obj']['array']) || !is_array($data['obj']['array'])){
            return [];
        }
        return array_map(function ($sns){
            $data = [];
            $data['sns_id'] = $sns['snsinfo']['id']??'';
            $data['userName'] = $sns['snsinfo']['username']??'';
            $data['create_time'] = intval($sns['snsinfo']['createTime']??0);

            $data['contentDesc'] = $sns['snsinfo']['contentDesc']??'';
            if (!is_string($data['contentDesc'])) $data['contentDesc'] = '';

            $data['title'] = $sns['snsinfo']['ContentObject']['title']??'';
            if (!is_string($data['title'])) $data['title'] = '';

            $data['description'] = $sns['snsinfo']['ContentObject']['description']??'';
            if (!is_string($data['description'])) $data['description'] = '';

            $data['contentUrl'] = $sns['snsinfo']['ContentObject']['contentUrl']??'';
            if (!is_string($data['contentUrl'])) $data['contentUrl'] = '';

            //朋友圈媒体信息
            $data['media'] = [];
            if (!empty($sns['snsinfo']['ContentObject']['mediaList']['media']['url'])){
                $data['media'][0] = $sns['snsinfo']['ContentObject']['mediaList']['media'];
            }elseif(!empty($sns['snsinfo']['ContentObject']['mediaList']['media'][0]['url'])){
                $data['media'] = $sns['snsinfo']['ContentObject']['mediaList']['media'];
            }
            $data['media'] = array_map(function ($media){
                $data = [];
                $data['thumb'] = $media['thumb']??'';
                $data['size'] = intval($media['size']['@attributes']['totalSize']??0);
                $data['width'] = intval($media['size']['@attributes']['width']??0);
                $data['height'] = intval($media['size']['@attributes']['height']??0);
                $data['videoDuration'] = intval($media['videoDuration']??0);
                return $data;
            },$data['media']);

            //视频号相关内容
            if (!empty($sns['snsinfo']['ContentObject']['finderFeed']['objectId'])){
                $params = ['objectId'=>"",'objectNonceId'=>"",'feedType'=>"",'nickname'=>"",'username'=>"",'avatar'=>"",'desc'=>""];
                foreach ($params as $key => $def){
                    $data['finderFeed'][$key] = $sns['snsinfo']['ContentObject']['finderFeed'][$key]??'';
                }
                //视频号媒体相关信息
                $data['finderFeed']['media'] = [];
                if (!empty($sns['snsinfo']['ContentObject']['finderFeed']['mediaList']['media']['url'])){
                    $data['finderFeed']['media'][0] = $sns['snsinfo']['ContentObject']['finderFeed']['mediaList']['media'];
                }elseif(!empty($sns['snsinfo']['ContentObject']['mediaList']['media'][0]['url'])){
                    $data['finderFeed']['media'] = $sns['snsinfo']['ContentObject']['mediaList']['media'];
                }
                $data['finderFeed']['media'] = array_map(function ($media){
                    $data = [];
                    $data['thumb'] = $media['thumbUrl']??'';
                    $data['width'] = intval($media['width']??'');
                    $data['height'] = intval($media['height']??'');
                    $data['videoDuration'] = intval($media['videoPlayDuration']??0);
                    return $data;
                },$data['finderFeed']['media']);
            }
            return $data;
        },$data['obj']['array']);
    }

    /**
     * @param $data
     * @return array
     */
    public function MessageSync($data): array
    {
        if (!empty($data['obj']['array'][0])){
            return array_reduce($data['obj']['array'],function ($carry,$data){
                if (!empty($data['info']['msgId'])){
                    $carry[] = $data;
                }
                return $carry;
            },[]);
        }
        return [];
    }

}