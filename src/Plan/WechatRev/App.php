<?php

namespace MicroCyanHelper\Plan\WechatRev;

use GuzzleHttp\Exception\GuzzleException;
use MicroCyanHelper\Core\Kernel\BaseCore;
use MicroCyanHelper\Plan\WechatRev\Helper\Filter;
use Psr\SimpleCache\InvalidArgumentException;


class App extends BaseCore
{
    use \MicroCyanHelper\Plan\WechatRev\Traits\Config;

    /**
     * @var Filter
     */
    public $filter = null;
    /**
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $config=[])
    {
        parent::__construct($config);
        $this->filter = new Filter();
    }

    /**
     * @description 测试
     * @return string
     * @throws GuzzleException
     * @throws InvalidArgumentException
     */
    public function test(){
        return $this->get_access_token();
    }

    /**
     * @param array $config 机器人的相关信息，token和代理
     * @return void
     * @throws GuzzleException
     * @throws InvalidArgumentException
     */
    public function getMessageSync(array $config)
    {
        if (!empty($config['token'])){
            $config['token'] = json_decode($config['token'],true);
            if (!empty($config['token']['uin'])){
                $cache_sync_key = $this->cache_sync_key.$config['token']['uin'];
            }
        }
        $sync = $this->http_post('/api/WxSync/Sync',$config);
        if (!empty($sync['obj']['syncKey'])&&!empty($cache_sync_key)){
            $this->cache->set($cache_sync_key,$sync['obj']['syncKey'],3600);
        }
        if (!empty($sync['obj']['array'])&&is_array($sync['obj']['array'])){
            $sync['obj']['array'] = array_reduce($sync['obj']['array'],function ($syncs,$sync){
                if (empty($sync['info'])) return $syncs;
                $sync['info'] = json_decode($sync['info'],true);
                if (!empty($sync['info']['content']['string'])) $sync['info']['content']['string'] = $this->xml2arr($sync['info']['content']['string']);
                if (!empty($sync['info']['msgSource'])) $sync['info']['msgSource'] = $this->xml2arr($sync['info']['msgSource']);
                $syncs [] = $sync;
                return $syncs;
            },[]);
        }
        return $sync;
    }


    /**
     * @description 获取个人资料
     * @param array $config
     * @return array|mixed
     * @throws GuzzleException
     * @throws InvalidArgumentException
     */
    public function getWxProfile(array $config,array $params = []){
        $withQrcode = $params['withQrcode']??false;
        if ($withQrcode!==true){
            $withQrcode = is_numeric($withQrcode)&&$withQrcode>0;
        }
        unset($params['withQrcode']);
        $profile = $this->http_post('/api/WxProfile/Profile',$config);
        if ($withQrcode === true&&!empty($profile['obj']['userInfo']['userName']['string'])){
            $qrcode = $this->getGetProfileQrCode($config,['id'=>$profile['obj']['userInfo']['userName']['string']]);
            if (!empty($qrcode['obj'])&&is_string($qrcode['obj'])&&preg_match("/jpg|png|jpeg$/i",$qrcode['obj'],$matches)){
                $qrcode['obj'] = $this->http->get($qrcode['obj'])->getBody()->getContents();
                $qrcode['obj'] = base64_encode($qrcode['obj']);
                $qrcode['obj'] = "data:image/$matches[0];base64,".$qrcode['obj'];
            }
            $profile['qrcode'] = $qrcode['obj']??'';
        }
        return $profile;
    }
    /**
     * @param array $config
     * @param array $params => id:获取个人传入 wxid_xxxx 获取群传入 xxx@chatroom
     * @return array|mixed
     * @throws GuzzleException
     * @throws InvalidArgumentException
     */
    public function getGetProfileQrCode(array $config,array $params){
        return $this->http_post('/api/WxProfile/GetProfileQrCode',$config,[
            'body'=>$params
        ]);
    }

    /**
     * @description 获取朋友圈内容
     * @param array $config
     * @param array $params  --- wxId 目标用户
     * @return array
     * @throws GuzzleException
     * @throws InvalidArgumentException
     */
    public function getSnsPage(array $config,array $params): array
    {
        $options = [];
        $options['body'] = $params;
        if (empty($options['body']['type'])) $options['body']['type'] = 1001;
        if (empty($options['body']['maxId'])) $options['body']['maxId'] = 0;
        $sns = $this->http_post('/api/WxSns/SnsPage',$config,$options);
        if (!empty($sns['obj']['array'])&&is_array($sns['obj']['array'])){
            $sns['obj']['array'] = array_map(function ($sns){
                if (!empty($sns['snsinfo'])) $sns['snsinfo'] = $this->xml2arr($sns['snsinfo']);
                return $sns;
            },$sns['obj']['array']??[]);
        }
        return $sns;
    }

    /**
     * @description 获取群信息
     * @param array $config
     * @param array $params --- id => 群的ID
     * @return array|mixed
     * @throws GuzzleException
     * @throws InvalidArgumentException
     */
    public function getChatRoomDetail(array $config,array $params){
        return $this->http_post('/api/WxChatRoom/RoomDetail',$config,[
            'body'=>$params
        ]);
    }

    /**
     * @description 获取群成员信息
     * @param array $config
     * @param array $params --- id => 群的ID
     * @return array|mixed
     * @throws GuzzleException
     * @throws InvalidArgumentException
     */
    public function getChatRoomMemberDetail(array $config,array $params){
        $options = [];
        $options['body'] = $params;
        if (empty($options['body']['clientVersion'])) $options['body']['clientVersion'] = 0;
        return $this->http_post('/api/WxChatRoom/RoomMemberDetail',$config,$options);
    }


}