<?php

namespace MicroCyanHelper\Plan\Tool;

use MicroCyanHelper\Plan\Tool\Helper\ObjectStorage\Minio;
use MicroCyanHelper\Plan\Tool\Helper\ObjectStorage\OSS;

class App
{
    /**
     * @var Minio|OSS
     */
    public $object_storage = null;
    public function __construct(array $config=[])
    {
        $ObjectStorageType = $config['ObjectStorage']['type']??'';
        if (stripos($ObjectStorageType,"minio")!==false){
            $this->object_storage = new Minio($config);
        }elseif (stripos($ObjectStorageType,"oss")!==false){
            $this->object_storage = new OSS($config);
        }
    }










}