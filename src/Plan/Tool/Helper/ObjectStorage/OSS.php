<?php

namespace MicroCyanHelper\Plan\Tool\Helper\ObjectStorage;

use OSS\OssClient;

class OSS
{
    /**
     * @var OssClient
     */
    protected $client = null;
    protected $endpoint = 'oss-cn-hangzhou.aliyuncs.com';
    /**
     * @var array
     */
    protected $config = [];
    /**
     * @param array $config
     */
    public function __construct(array $config=[]){
        $this->config = $config??[];
        $accessKeyId = $this->config['ObjectStorage']['credentials']['key']??'';
        $accessKeySecret = $this->config['ObjectStorage']['credentials']['secret']??'';
        $endpoint = $this->config['ObjectStorage']['endpoint']??'';
        $endpoint = preg_replace('/\/{1,}$/',"",$endpoint);
        $endpoint = preg_replace('/^(https?\:\/\/)+/',"",$endpoint);
        if (!empty($endpoint))$this->endpoint = $endpoint;
        $this->client = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
    }
    public function putObject($filename,$content){
        $bucket = $this->config['ObjectStorage']['bucket']??'';
        $url = $this->config['ObjectStorage']['url']??'';
        $url = preg_replace('/\/{1,}$/',"",$url);
        $puttedURL = $this->client->putObject($bucket, $filename, $content);
        $puttedURL = $puttedURL['info']['url']??'';
        if (empty($puttedURL)) throw new \Exception('上传失败');
        if (!empty($url)){
            $puttedURL = preg_replace("/\/{2,}/",'/',"{$url}/$filename");
            $puttedURL = str_replace(':/','://',$puttedURL);
        }
        return $puttedURL;
    }
    public function getObjects(array $params,bool $url = true): array
    {
        $bucket = $this->config['ObjectStorage']['bucket']??'';
        if ($url){
            $url = $this->config['ObjectStorage']['url']??'';
            $url = preg_replace('/\/{1,}$/',"",$url);
        }else{
            $url = '';
        }
        $params['delimiter'] = '';
        foreach ($params as $paramKey => $paramValue) {
            unset($params[$paramKey]);
            $params[lcfirst($paramKey)]=$paramValue;
        }
        $objectInfo = $this->client->listObjects($bucket,$params);
        $objects = $objectInfo->getObjectList();
        $cb_objects = [];
        $cb_objects['EditVersion'] = [];
        $cb_objects['Contents'] = [];
        foreach ($objects as $index => $content){
            $cb_objects['EditVersion'][] = $content->getLastModified();
            $cb_objects['Contents'][$index] = [];
            $cb_objects['Contents'][$index]['Key'] = $content->getKey();
            $cb_objects['Contents'][$index]['LastModified'] = $content->getLastModified();
            $cb_objects['Contents'][$index]['Size'] = $content->getSize();
            $cb_objects['Contents'][$index]['ETag'] = $content->getETag();
            $cb_objects['Contents'][$index]['Type'] = $content->getType();
            $cb_objects['Contents'][$index]['StorageClass'] = $content->getStorageClass();
            if (!empty($url)){
                $cb_objects['Contents'][$index]['URL'] = "{$url}/{$content->getKey()}";
            }else{
                $cb_objects['Contents'][$index]['URL'] = "https://{$bucket}.{$this->endpoint}/{$content->getKey()}";
            }
            $cb_objects['Contents'][$index]['URL'] = preg_replace("/\/{2,}/",'/',$cb_objects['Contents'][$index]['URL']);
            $cb_objects['Contents'][$index]['URL'] = str_replace(":/",'://',$cb_objects['Contents'][$index]['URL']);
        }
        sort($cb_objects['EditVersion']);
        $cb_objects['EditVersion'] = md5(serialize($cb_objects['EditVersion']));
        return $cb_objects;
    }

}