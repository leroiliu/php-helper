<?php
/**
 * Created By Leroi Liu
 * Email：675667823@qq.com
 * Date：2023/2/12
 * Description：
 **/
if (class_exists('\think\facade\Db')){

    if (!function_exists('model')){
        function model($table=''){
            return \think\facade\Db::table($table);
        }
    }

    if (!function_exists('json_success')){
        function json_success($data=[],$message='结果请求获取成功',$code=200,$toast=0){
            return json(['code'=>$code,'message'=>$message,'data'=>$data,'toast'=>$toast]);
        }
    }

    if (!function_exists('json_error')){
        function json_error($message='结果请求获取失败',$data=[],$code=500,$toast=1){
            return json(['code'=>$code,'message'=>$message,'data'=>$data,'toast'=>$toast]);
        }
    }

    if (!function_exists('exit_error')) {
        function exit_error($message = '结果请求获取失败', $data = [], $code = 500,$toast=1)
        {
            $header['Access-Control-Allow-Origin']  = app('request')->header('origin');
            $header['Access-Control-Allow-Headers'] = 'Access-Control-Allow-Headers: Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-CSRF-TOKEN, X-Requested-With, Token';
            $header['Access-Control-Max-Age'] = '1800';
            $header['Access-Control-Allow-Credentials']  = 'true';
            $header['Access-Control-Allow-Methods']  = 'GET, POST, PATCH, PUT, DELETE, OPTIONS';
            $response                               = \think\Response::create(['code' => $code, 'message'  => $message, 'data' => $data, 'toast' => $toast], 'json')->header($header);
            throw new \think\exception\HttpResponseException($response);exit();
        }
    }

    if (!function_exists('createToken')){
        //全局创建 Token 方法
        function createToken($value,$type,$expire=3600*24): string
        {
            $_value = $value;
            if (is_array($_value)){ $_value = json_encode($_value);}
            $token = $type.':'.md5($type.':'.$_value.':'.time());
            \think\facade\Cache::set($token,$value,$expire);
            return $token;
        }
    }


}