<?php

namespace MicroCyanHelper\Exception;

class HelperException extends \Exception
{

    // 重定义构造器使 message 变为必须被指定的属性
    public function __construct($message, $code = 500, \Throwable $previous = null)
    {
        // 这里写用户的代码

        // 确保所有变量都被正确赋值
        parent::__construct($message, $code, $previous);
    }

}
