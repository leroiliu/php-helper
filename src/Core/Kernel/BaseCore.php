<?php

namespace MicroCyanHelper\Core\Kernel;

class BaseCore
{
    use \MicroCyanHelper\Core\Kernel\Traits\Redis;
    use \MicroCyanHelper\Core\Kernel\Traits\Http;
    use \MicroCyanHelper\Core\Kernel\Traits\Xml;
    /**
     * @var array 基础配置信息
     */
    protected $config = [];
    /**
     * @var string
     */
    protected $namespace = '';

    /**
     * @var \MicroCyanHelper\Core\Kernel\Traits\Cache
     */
    protected $cache = null;

    /**
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $config=[])
    {
        //验证当前应用的namespace
        $namespace = $config['namespace']??'';
        if (is_string($namespace)&&preg_match("/^[\w:]{6,50}$/",$namespace)){
            $this->namespace = $namespace;
        }else{
            throw new \Exception('未配置脚本命名名空间，请配置脚本命名名空间，由字母、数字、下划线及英文":"组成，总长度再6到50之间');
        }
        //配置设置
        $this->config = $config;
        //设置redis实例
        $this->setRedis();
        //设置缓存
        $this->cache = new \MicroCyanHelper\Core\Kernel\Traits\Cache($config,$this->redis);
        //设置请求http实例
        $this->setHttp();

    }





}