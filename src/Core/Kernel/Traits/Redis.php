<?php

namespace MicroCyanHelper\Core\Kernel\Traits;

trait Redis
{

    /**
     * @var \Predis\Client
     */
    protected $redis = null;
    private function setRedis(){
        if (empty($this->config['redis'])) throw new \Exception('暂未配置Redis');
        if ($this->redis instanceof \Predis\Client){
            return $this->redis;
        }else if ($this->config['redis'] instanceof \Predis\Client){
            $this->redis = $this->config['redis'];
            return $this->redis;
        }else if (!empty($this->config['redis']['host'])){
            //redis连接配置
            $tcp = 'tcp://';
            $tcp.= $this->config['redis']['host'];
            $tcp.= ':';
            $tcp.= $this->config['redis']['port']??6379;
            //选择select的库
            $select = $this->config['redis']['select']??0;
            if (!is_numeric($select) || $select<0 ) $select = 0;
            //新增对象
            $predis = new \Predis\Client($tcp);
            $predis->auth($this->config['redis']['password']??'');
            $predis->select($select);
            $this->redis = $predis;
            return $this->redis;
        }else{
            throw new \Exception('抱歉，暂无未进行Redis配置');
        }
    }

}