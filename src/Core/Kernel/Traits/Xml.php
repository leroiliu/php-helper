<?php

namespace MicroCyanHelper\Core\Kernel\Traits;

trait Xml
{
    /**
     * @description 将xml数据转化为数组数据
     * @param $xmlStr
     * @return array|mixed|string
     */
    public function xml2arr($xmlStr){
        try {
            if (is_string($xmlStr)){
                if (preg_match("#<(\w+).*<\s*\/\s*(\w+)\s*>#si",$xmlStr,$matches)){
                    if ($matches[1]===$matches[2]){
                        $loadXmlStr = simplexml_load_string($matches[0],'SimpleXMLElement',LIBXML_NOCDATA);
                        $loadXmlStr = json_decode(json_encode( $loadXmlStr,JSON_UNESCAPED_UNICODE),true);
                        if (is_array($loadXmlStr)&&!empty($loadXmlStr)){
                            return $loadXmlStr;
                        }
                    }
                }
            }
        }catch (\Exception $exception){

        }
        return $xmlStr;
    }
}