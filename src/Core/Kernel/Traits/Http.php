<?php

namespace MicroCyanHelper\Core\Kernel\Traits;

use GuzzleHttp\Client;

trait Http
{

    /**
     * @var Client
     */
    protected $http = null;

    /**
     * @return Client
     */
    private function setHttp(): Client
    {
        if ($this->http instanceof Client){
            return $this->http;
        }else if (!empty($this->config['http']) && $this->config['http'] instanceof Client){
            $this->http = $this->config['http'];
            return $this->http;
        }else{
            $options = [];
            if (!empty($this->config['http']) && is_array($this->config['http'])){
                $options = $this->config['http'];
            }
            $this->http = new Client($options);
            return $this->http;
        }
    }

}