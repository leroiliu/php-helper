<?php

namespace MicroCyanHelper\Core;

/**
 * @method static \MicroCyanHelper\Plan\WechatRev\App WechatRev(array $config)
 * @method static \MicroCyanHelper\Plan\HttpCrontab\App HttpCrontab(array $config)
 * @method static \MicroCyanHelper\Plan\KuaiTuanTuan\App KuaiTuanTuan(array $config)
 * @method static \MicroCyanHelper\Plan\Tool\App Tool(array $config)
 */
class Factory
{

    /**
     * @param $name
     * @param $arguments
     * @return \MicroCyanHelper\Plan\WechatRev\App|\MicroCyanHelper\Plan\HttpCrontab\App|\MicroCyanHelper\Plan\KuaiTuanTuan\App|\MicroCyanHelper\Plan\Tool\App
     * @throws \Exception
     */
    public static function __callStatic($name, $arguments)
    {
        // TODO: Implement __callStatic() method.
        error_reporting(E_ALL^E_NOTICE^E_WARNING^E_DEPRECATED);
        $name = strtolower($name);
        $name = str_replace('_','',$name);
        if ($name==='wechatrev'){
            return new \MicroCyanHelper\Plan\WechatRev\App(...$arguments);
        }elseif ($name==='httpcrontab'){
            return new \MicroCyanHelper\Plan\HttpCrontab\App(...$arguments);
        }elseif ($name==='kuaituantuan'){
            return new \MicroCyanHelper\Plan\KuaiTuanTuan\App(...$arguments);
        }elseif ($name==='tool'){
            return new \MicroCyanHelper\Plan\Tool\App(...$arguments);
        }
        throw new \Exception('抱歉，暂无该方法');
    }


}